#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
This file defines:
    * a class that encapsulates the original OSeMOSYS ABSTRACT model definition in python using Pyomo,
    * a class that encapsulates a CONCRETE model built from an OSeMOSYS abstract model with 
      additional methods (e.g. to solve the model, export all variables to csv).
      
The original code comes from the file 'OSeMOSYS_2015_08_27_Pyomo.py' downloaded
from the GitHub repository https://github.com/KTH-dESA/OSeMOSYS.

Modifications to the original code:
    * Update the python code using the GNU MathProg OSeMOSYS code (the python code 
      was using outdated naming of the constraints and was missing a few such as
      the storage constraint or the RE production targets equations).
    * Remove the left-over GNU MathProg code commented out at the end of the file.
    * Add class definition, constructor, and indent code accordingly.
    * Append 'self.' to attribute definitions (parameters, variables, constraints)
      using pyomo Abstractmodel instance 'model'.
    * Add 'self' as first argument in method definitions (constraint and objective functions).
    * Separate constraint function definitions as methods from building pyomo
      Constraint objects as attributes in the constructor.
    * Add method for loading data from csv files into Sets and Params of abstract model
      using a pyomo DataPortal object.
    * Add a class encapsulating concrete OSeMOSYS models built with pyomo. The class provides 
      methods to solve the model, export the LP problem definition, and export all variables to csv files.
'''

# OSeMOSYS_2015_08_27
# 
# Open Source energy modeling SYStem
#
# Main changes to previous version OSeMOSYS_2013_05_10
#		- Removed the parameter TechWithCapacityNeededToMeetPeakTS from constraint CAa4_Constraint_Capacity
#		- Fixed a bug related to using CapacityOfOneTechnologyUnit in constraint CAa5_TotalNewCapacity
#		- Fixed a bug in the storage equations which caused an error if more than one day type was used 
#		- DiscountRate is no longer technology-specific. Therefore, DiscountRateStorage is now replaced by DiscountRate.
#
# ============================================================================
#
#    Copyright [2010-2015] [OSeMOSYS Forum steering committee see: www.osemosys.org]
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
# ============================================================================
#

__all__ = ('abstract_OSeMOSYS',
           'concrete_OSeMOSYS')

#from __future__ import division
import os
from pyomo.environ import AbstractModel, DataPortal, Set, Param, Var, Objective, Constraint, NonNegativeReals, NonNegativeIntegers, minimize, value
from pyomo.opt import SolverFactory

##############################################################################

class abstract_OSeMOSYS(object):
    '''
    Class encapsulating the original OSeMOSYS abstract model definition using pyomo.
    
    Constructor arguments:
        InputPath [optional]: string
            Path to directory of csv input data files. Default: None.

    Public class attributes:
        model: Pyomo AbstractModel object
            Instance of the Pyomo AbstractModel class. Parameter values are unspecified 
            and will be supplied to a concrete model instance (see Class concrete_OSeMOSYS)
            when a solution is to be obtained.
        data: Pyomo DataPortal object
            Instance of the Pyomo DataPortal class. Data from csv files (located 
            at InputPath) can be loaded into this object with the public method load_data().
        InputPath: string
           Path to directory of csv input data files.
    '''
    def __init__(self, InputPath=None):
        
        # Instantiate pyomo's AbstractModel
        self.model = AbstractModel()
        # Instantiate pyomo's DataPortal
        self.data = DataPortal()
        # Path to directory of csv input data files (optional)
        self.InputPath = InputPath
        
        ###############
        #    Sets     #
        ############### 
        
        self.model.YEAR = Set()
        self.model.TECHNOLOGY = Set()
        self.model.TIMESLICE = Set()
        self.model.FUEL = Set()
        self.model.EMISSION = Set()
        self.model.MODE_OF_OPERATION = Set()
        self.model.REGION = Set()
        self.model.SEASON = Set()
        self.model.DAYTYPE = Set()
        self.model.DAILYTIMEBRACKET = Set()
        self.model.FLEXIBLEDEMANDTYPE = Set()
        self.model.STORAGE = Set()
        
        #####################
        #    Parameters     #
        #####################
        
        ########			Global 						#############
        
        self.model.YearSplit = Param(self.model.TIMESLICE, self.model.YEAR)
        self.model.DiscountRate = Param(self.model.REGION, default=0.05)
        self.model.DaySplit = Param(self.model.DAILYTIMEBRACKET, self.model.YEAR, default=0.00137)
        self.model.Conversionls = Param(self.model.TIMESLICE, self.model.SEASON, default=0)
        self.model.Conversionld = Param(self.model.TIMESLICE, self.model.DAYTYPE, default=0)
        self.model.Conversionlh = Param(self.model.TIMESLICE, self.model.DAILYTIMEBRACKET, default=0)
        self.model.DaysInDayType = Param(self.model.SEASON, self.model.DAYTYPE, self.model.YEAR, default=7)
        self.model.TradeRoute = Param(self.model.REGION, self.model.REGION, self.model.FUEL, self.model.YEAR, default=0)
        self.model.DepreciationMethod = Param(self.model.REGION, default=1)
        
        ########			Demands 					#############
        
        self.model.SpecifiedAnnualDemand = Param(self.model.REGION, self.model.FUEL, self.model.YEAR, default=0)
        self.model.SpecifiedDemandProfile = Param(self.model.REGION, self.model.FUEL, self.model.TIMESLICE, self.model.YEAR, default=0)
        self.model.AccumulatedAnnualDemand = Param(self.model.REGION, self.model.FUEL, self.model.YEAR, default=0)
        
        #########			Performance					#############
        
        self.model.CapacityToActivityUnit = Param(self.model.REGION, self.model.TECHNOLOGY, default=1)
        self.model.CapacityFactor = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.TIMESLICE, self.model.YEAR, default=1)
        self.model.AvailabilityFactor = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=1)
        self.model.OperationalLife = Param(self.model.REGION, self.model.TECHNOLOGY, default=1)
        self.model.ResidualCapacity = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=0)
        self.model.InputActivityRatio = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.FUEL, self.model.MODE_OF_OPERATION, self.model.YEAR, default=0)
        self.model.OutputActivityRatio = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.FUEL, self.model.MODE_OF_OPERATION, self.model.YEAR, default=0)
        
        #########			Technology Costs			#############
        
        self.model.CapitalCost = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=0)
        self.model.VariableCost = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.MODE_OF_OPERATION, self.model.YEAR, default=0.00001)
        self.model.FixedCost = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=0)
        
        #########           		Storage                 		#############
        
        self.model.TechnologyToStorage = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.STORAGE, self.model.MODE_OF_OPERATION, default=0)
        self.model.TechnologyFromStorage = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.STORAGE, self.model.MODE_OF_OPERATION, default=0)
        self.model.StorageLevelStart = Param(self.model.REGION, self.model.STORAGE, default=999)
        self.model.StorageMaxChargeRate = Param(self.model.REGION, self.model.STORAGE, default=99)
        self.model.StorageMaxDischargeRate = Param(self.model.REGION, self.model.STORAGE, default=99)
        self.model.MinStorageCharge = Param(self.model.REGION, self.model.STORAGE, self.model.YEAR, default=0)
        self.model.OperationalLifeStorage = Param(self.model.REGION, self.model.STORAGE, default=99)
        self.model.CapitalCostStorage = Param(self.model.REGION, self.model.STORAGE, self.model.YEAR, default=0)
        self.model.ResidualStorageCapacity = Param(self.model.REGION, self.model.STORAGE, self.model.YEAR, default=0)
        
        #########			Capacity Constraints		#############
        
        self.model.CapacityOfOneTechnologyUnit = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=0)
        self.model.TotalAnnualMaxCapacity = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=99999)
        self.model.TotalAnnualMinCapacity = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=0)
        
        #########			Investment Constraints		#############
        
        self.model.TotalAnnualMaxCapacityInvestment = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=99999)
        self.model.TotalAnnualMinCapacityInvestment = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=0)
        
        #########			Activity Constraints		#############
        
        self.model.TotalTechnologyAnnualActivityUpperLimit = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=99999)
        self.model.TotalTechnologyAnnualActivityLowerLimit = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=0)
        self.model.TotalTechnologyModelPeriodActivityUpperLimit = Param(self.model.REGION, self.model.TECHNOLOGY, default=99999)
        self.model.TotalTechnologyModelPeriodActivityLowerLimit = Param(self.model.REGION, self.model.TECHNOLOGY, default=0)
        
        #########			Reserve Margin				############# 
        
        self.model.ReserveMarginTagTechnology = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=0)
        self.model.ReserveMarginTagFuel = Param(self.model.REGION, self.model.FUEL, self.model.YEAR, default=0)
        self.model.ReserveMargin = Param(self.model.REGION, self.model.YEAR, default=1)
        
        #########			RE Generation Target		############# 
        
        self.model.RETagTechnology = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, default=0)
        self.model.RETagFuel = Param(self.model.REGION, self.model.FUEL, self.model.YEAR, default=0)
        self.model.REMinProductionTarget = Param(self.model.REGION, self.model.YEAR, default=0)
        
        #########			Emissions & Penalties		#############
        
        self.model.EmissionActivityRatio = Param(self.model.REGION, self.model.TECHNOLOGY, self.model.EMISSION, self.model.MODE_OF_OPERATION, self.model.YEAR, default=0)
        self.model.EmissionsPenalty = Param(self.model.REGION, self.model.EMISSION, self.model.YEAR, default=0)
        self.model.AnnualExogenousEmission = Param(self.model.REGION, self.model.EMISSION, self.model.YEAR, default=0)
        self.model.AnnualEmissionLimit = Param(self.model.REGION, self.model.EMISSION, self.model.YEAR, mutable=True, default=99999) # Param(mutable=True) allows to change the value of this parameter dynamically after the parameter has been constructed.
        self.model.ModelPeriodExogenousEmission = Param(self.model.REGION, self.model.EMISSION, default=0)
        self.model.ModelPeriodEmissionLimit = Param(self.model.REGION, self.model.EMISSION, mutable=True, default=99999) # Param(mutable=True) allows to change the value of this parameter dynamically after the parameter has been constructed.
        
        ######################
        #   Model Variables  #
        ######################
        
        ########			Demands 					#############
        
        self.model.RateOfDemand = Var(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.Demand = Var(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        
        ########     		Storage                 		#############
        
        self.model.RateOfStorageCharge = Var(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, initialize=0.0)
        self.model.RateOfStorageDischarge = Var(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, initialize=0.0)
        self.model.NetChargeWithinYear = Var(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, initialize=0.0)
        self.model.NetChargeWithinDay = Var(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, initialize=0.0)
        self.model.StorageLevelYearStart = Var(self.model.REGION, self.model.STORAGE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.StorageLevelYearFinish = Var(self.model.REGION, self.model.STORAGE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.StorageLevelSeasonStart = Var(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.StorageLevelDayTypeStart = Var(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.StorageLevelDayTypeFinish = Var(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.StorageLowerLimit = Var(self.model.REGION, self.model.STORAGE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.StorageUpperLimit = Var(self.model.REGION, self.model.STORAGE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.AccumulatedNewStorageCapacity = Var(self.model.REGION, self.model.STORAGE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.NewStorageCapacity = Var(self.model.REGION, self.model.STORAGE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.CapitalInvestmentStorage = Var(self.model.REGION, self.model.STORAGE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.DiscountedCapitalInvestmentStorage = Var(self.model.REGION, self.model.STORAGE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.SalvageValueStorage = Var(self.model.REGION, self.model.STORAGE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.DiscountedSalvageValueStorage = Var(self.model.REGION, self.model.STORAGE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.TotalDiscountedStorageCost = Var(self.model.REGION, self.model.STORAGE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        
        #########		    Capacity Variables 			############# 
        
        self.model.NumberOfNewTechnologyUnits = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeIntegers, initialize=0)
        self.model.NewCapacity = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.AccumulatedNewCapacity = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.TotalCapacityAnnual = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        
        #########		    Activity Variables 			#############
        
        self.model.RateOfActivity = Var(self.model.REGION, self.model.TIMESLICE, self.model.TECHNOLOGY, self.model.MODE_OF_OPERATION, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.RateOfTotalActivity = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.TIMESLICE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.TotalTechnologyAnnualActivity = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.TotalAnnualTechnologyActivityByMode = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.MODE_OF_OPERATION, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.RateOfProductionByTechnologyByMode = Var(self.model.REGION, self.model.TIMESLICE, self.model.TECHNOLOGY, self.model.MODE_OF_OPERATION, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.RateOfProductionByTechnology = Var(self.model.REGION, self.model.TIMESLICE, self.model.TECHNOLOGY, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.ProductionByTechnology = Var(self.model.REGION, self.model.TIMESLICE, self.model.TECHNOLOGY, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.ProductionByTechnologyAnnual = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.RateOfProduction = Var(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.Production = Var(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.RateOfUseByTechnologyByMode = Var(self.model.REGION, self.model.TIMESLICE, self.model.TECHNOLOGY, self.model.MODE_OF_OPERATION, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.RateOfUseByTechnology = Var(self.model.REGION, self.model.TIMESLICE, self.model.TECHNOLOGY, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.UseByTechnologyAnnual = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.RateOfUse = Var(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.UseByTechnology = Var(self.model.REGION, self.model.TIMESLICE, self.model.TECHNOLOGY, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.Use = Var(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.Trade = Var(self.model.REGION, self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, initialize=0.0)
        self.model.TradeAnnual = Var(self.model.REGION, self.model.REGION, self.model.FUEL, self.model.YEAR, initialize=0.0)
        
        self.model.ProductionAnnual = Var(self.model.REGION, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.UseAnnual = Var(self.model.REGION, self.model.FUEL, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        
        
        #########		    Costing Variables 			#############
        
        self.model.CapitalInvestment = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.DiscountedCapitalInvestment = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        
        self.model.SalvageValue = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.DiscountedSalvageValue = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.OperatingCost = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.DiscountedOperatingCost = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        
        self.model.AnnualVariableOperatingCost = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.AnnualFixedOperatingCost = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.VariableOperatingCost = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.TIMESLICE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        
        self.model.TotalDiscountedCostByTechnology = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.TotalDiscountedCost = Var(self.model.REGION, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        
        self.model.ModelPeriodCostByRegion = Var(self.model.REGION, domain=NonNegativeReals, initialize=0.0)
        
        #########			Reserve Margin				#############
        
        self.model.TotalCapacityInReserveMargin = Var(self.model.REGION, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.DemandNeedingReserveMargin = Var(self.model.REGION,self.model.TIMESLICE, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        
        #########			RE Gen Target				#############
        
        self.model.TotalREProductionAnnual = Var(self.model.REGION, self.model.YEAR, initialize=0.0)
        self.model.RETotalDemandOfTargetFuelAnnual = Var(self.model.REGION, self.model.YEAR, initialize=0.0)
        
        self.model.TotalTechnologyModelPeriodActivity = Var(self.model.REGION, self.model.TECHNOLOGY, initialize=0.0)
        
        #########			Emissions					#############
        
        self.model.AnnualTechnologyEmissionByMode = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.EMISSION, self.model.MODE_OF_OPERATION, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.AnnualTechnologyEmission = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.EMISSION, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.AnnualTechnologyEmissionPenaltyByEmission = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.EMISSION, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.AnnualTechnologyEmissionsPenalty = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.DiscountedTechnologyEmissionsPenalty = Var(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.AnnualEmissions = Var(self.model.REGION, self.model.EMISSION, self.model.YEAR, domain=NonNegativeReals, initialize=0.0)
        self.model.ModelPeriodEmissions = Var(self.model.REGION, self.model.EMISSION, domain=NonNegativeReals, initialize=0.0)
    
        
        ######################
        # Objective Function #
        ######################
        
        
        self.model.OBJ = Objective(rule=self.ObjectiveFunction_rule, sense=minimize, doc='min_costs')
        
        
        #####################
        # Constraints       #
        #####################
        
        
        self.model.EQ_SpecifiedDemand = Constraint(self.model.REGION, self.model.FUEL, self.model.TIMESLICE, self.model.YEAR, rule=self.EQ_SpecifiedDemand_rule)
        
        
        #########       	Capacity Adequacy A	     	#############
        
        	
        self.model.CAa1_TotalNewCapacity_1 = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.CAa1_TotalNewCapacity_1_rule)
        	
        self.model.CAa2_TotalAnnualCapacity_constraint = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.CAa2_TotalAnnualCapacity_rule)
        
        self.model.CAa3_TotalActivityOfEachTechnology = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.TIMESLICE, self.model.YEAR, rule=self.CAa3_TotalActivityOfEachTechnology_rule)
        
        self.model.CAa4_ConstraintCapacity = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.TECHNOLOGY, self.model.YEAR, rule=self.CAa4_ConstraintCapacity_rule)
        
#        self.model.CAa5_TotalNewCapacity = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.CAa5_TotalNewCapacity_rule)      
        

        #########       	Capacity Adequacy B		 	#############
        
        
        self.model.CAb1_PlannedMaintenance = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.CAb1_PlannedMaintenance_rule)
        
        
        #########	        Energy Balance A    	 	#############
        
        
        self.model.EBa1_RateOfFuelProduction1 = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.TECHNOLOGY, self.model.MODE_OF_OPERATION, self.model.YEAR, rule=self.EBa1_RateOfFuelProduction1_rule)
               
        self.model.EBa2_RateOfFuelProduction2 = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.TECHNOLOGY, self.model.YEAR, rule=self.EBa2_RateOfFuelProduction2_rule)
        
        self.model.EBa3_RateOfFuelProduction3 = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, rule=self.EBa3_RateOfFuelProduction3_rule)
        
        self.model.EBa4_RateOfFuelUse1 = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.TECHNOLOGY, self.model.MODE_OF_OPERATION, self.model.YEAR, rule=self.EBa4_RateOfFuelUse1_rule)
        
        self.model.EBa5_RateOfFuelUse2 = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.TECHNOLOGY, self.model.YEAR, rule=self.EBa5_RateOfFuelUse2_rule)
        
        self.model.EBa6_RateOfFuelUse3 = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, rule=self.EBa6_RateOfFuelUse3_rule)
        
        self.model.EBa7_EnergyBalanceEachTS1 = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, rule=self.EBa7_EnergyBalanceEachTS1_rule)
        
        self.model.EBa8_EnergyBalanceEachTS2 = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, rule=self.EBa8_EnergyBalanceEachTS2_rule)
        
        self.model.EBa9_EnergyBalanceEachTS3 = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, rule=self.EBa9_EnergyBalanceEachTS3_rule)
        
        self.model.EBa10_EnergyBalanceEachTS4 = Constraint(self.model.REGION, self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, rule=self.EBa10_EnergyBalanceEachTS4_rule)
        
        self.model.EBa11_EnergyBalanceEachTS5 = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.FUEL, self.model.YEAR, rule=self.EBa11_EnergyBalanceEachTS5_rule)
        
        
        #########        	Energy Balance B		 	#############
        
        
        self.model.EBb1_EnergyBalanceEachYear1 = Constraint(self.model.REGION, self.model.FUEL, self.model.YEAR, rule=self.EBb1_EnergyBalanceEachYear1_rule)
        
        self.model.EBb2_EnergyBalanceEachYear2 = Constraint(self.model.REGION, self.model.FUEL, self.model.YEAR, rule=self.EBb2_EnergyBalanceEachYear2_rule)
        
        self.model.EBb3_EnergyBalanceEachYear3 = Constraint(self.model.REGION, self.model.REGION, self.model.FUEL, self.model.YEAR, rule=self.EBb3_EnergyBalanceEachYear3_rule)
        	
        self.model.EBb4_EnergyBalanceEachYear4 = Constraint(self.model.REGION, self.model.FUEL, self.model.YEAR, rule=self.EBb4_EnergyBalanceEachYear4_rule)
        
        
        #########        	Accounting Technology Production/Use	#############
        
        	
        self.model.Acc1_FuelProductionByTechnology = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.TECHNOLOGY, self.model.FUEL, self.model.YEAR, rule=self.Acc1_FuelProductionByTechnology_rule)
        
        self.model.Acc2_FuelUseByTechnology = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.TECHNOLOGY, self.model.FUEL, self.model.YEAR, rule=self.Acc2_FuelUseByTechnology_rule)
        
        self.model.Acc3_AverageAnnualRateOfActivity = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.MODE_OF_OPERATION, self.model.YEAR, rule=self.Acc3_AverageAnnualRateOfActivity_rule)
        
        self.model.Acc4_ModelPeriodCostByRegion_constraint = Constraint(self.model.REGION, rule=self.Acc4_ModelPeriodCostByRegion_rule)
        
        # self.model.Acc5_ModelPeriodCost = Constraint(rule=self.Acc5_ModelPeriodCost_rule)
        
        
            #########        	Storage Equations			#############
        
        
        self.model.S1_RateOfStorageCharge = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.S1_RateOfStorageCharge_rule)
    
        self.model.S2_RateOfStorageDischarge = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.S2_RateOfStorageDischarge_rule)
    
        self.model.S3_NetChargeWithinYear = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.S3_NetChargeWithinYear_rule)
    
        self.model.S4_NetChargeWithinDay = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.S4_NetChargeWithinDay_rule)
    
        self.model.S5_and_S6_StorageLevelYearStart = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.S5_and_S6_StorageLevelYearStart_rule)
    
        self.model.S7_and_S8_StorageLevelYearFinish = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.S7_and_S8_StorageLevelYearFinish_rule)
    
        self.model.S9_and_S10_StorageLevelSeasonStart = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.YEAR, rule=self.S9_and_S10_StorageLevelSeasonStart_rule)
        
        self.model.S11_and_S12_StorageLevelDayTypeStart = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.YEAR, rule=self.S11_and_S12_StorageLevelDayTypeStart_rule)
        
        self.model.S13_and_S14_and_S15_StorageLevelDayTypeFinish = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.YEAR, rule=self.S13_and_S14_and_S15_StorageLevelDayTypeFinish_rule)
        
        
            ##########		Storage Constraints				#############
        
        
        self.model.SC1_LowerLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInFirstWeekConstraint = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.SC1_LowerLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInFirstWeekConstraint_rule)
        
        self.model.SC1_UpperLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInFirstWeekConstraint = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.SC1_UpperLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInFirstWeekConstraint_rule)
    
        self.model.SC2_LowerLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInFirstWeekConstraint = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.SC2_LowerLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInFirstWeekConstraint_rule)
    
        self.model.SC2_UpperLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInFirstWeekConstraint = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.SC2_UpperLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInFirstWeekConstraint_rule)
    
        self.model.SC3_LowerLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInLastWeekConstraint = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.SC3_LowerLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInLastWeekConstraint_rule)
    
        self.model.SC3_UpperLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInLastWeekConstraint = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.SC3_UpperLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInLastWeekConstraint_rule)
        
        self.model.SC4_LowerLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInLastWeekConstraint = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.SC4_LowerLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInLastWeekConstraint_rule)
        
        self.model.SC4_UpperLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInLastWeekConstraint = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.SC4_UpperLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInLastWeekConstraint_rule)
    
        self.model.SC5_MaxChargeConstraint = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.SC5_MaxChargeConstraint_rule)
        
        self.model.SC6_MaxDischargeConstraint = Constraint(self.model.REGION, self.model.STORAGE, self.model.SEASON, self.model.DAYTYPE, self.model.DAILYTIMEBRACKET, self.model.YEAR, rule=self.SC6_MaxDischargeConstraint_rule)
        
        
        #########		Storage Investments				#############
        
        
        self.model.SI1_StorageUpperLimit = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.SI1_StorageUpperLimit_rule)
        
        self.model.SI2_StorageLowerLimit = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.SI2_StorageLowerLimit_rule)
        
        self.model.SI3_TotalNewStorage = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.SI3_TotalNewStorage_rule)
        
        self.model.SI4_UndiscountedCapitalInvestmentStorage = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.SI4_UndiscountedCapitalInvestmentStorage_rule)
        
        self.model.SI5_DiscountingCapitalInvestmentStorage = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.SI5_DiscountingCapitalInvestmentStorage_rule)
        
        self.model.SI6_SalvageValueStorageAtEndOfPeriod1 = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.SI6_SalvageValueStorageAtEndOfPeriod1_rule)
        
        self.model.SI7_SalvageValueStorageAtEndOfPeriod2 = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.SI7_SalvageValueStorageAtEndOfPeriod2_rule)
        
        self.model.SI8_SalvageValueStorageAtEndOfPeriod3 = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.SI8_SalvageValueStorageAtEndOfPeriod3_rule)
        
        self.model.SI9_SalvageValueStorageDiscountedToStartYear = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.SI9_SalvageValueStorageDiscountedToStartYear_rule)
        
        self.model.SI10_TotalDiscountedCostByStorage = Constraint(self.model.REGION, self.model.STORAGE, self.model.YEAR, rule=self.SI10_TotalDiscountedCostByStorage_rule)
    
        
        #########       	Capital Costs 		     	#############
        
        
        self.model.CC1_UndiscountedCapitalInvestment = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.CC1_UndiscountedCapitalInvestment_rule)
        
        self.model.CC2_DiscountedCapitalInvestment_constraint = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.CC2_DiscountedCapitalInvestment_rule)
        
        
        #########           Salvage Value            	#############
        
        
        self.model.SV1_SalvageValueAtEndOfPeriod1 = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.SV1_SalvageValueAtEndOfPeriod1_rule)
        
        self.model.SV4_SalvageValueDiscountedToStartYear = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.SV4_SalvageValueDiscountedToStartYear_rule)
        
        
        #########        	Operating Costs 		 	#############
        
        
        self.model.OC1_OperatingCostsVariable = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.TIMESLICE, self.model.YEAR, rule=self.OC1_OperatingCostsVariable_rule)
        
        self.model.OC2_OperatingCostsFixedAnnual = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.OC2_OperatingCostsFixedAnnual_rule)
        
        self.model.OC3_OperatingCostsTotalAnnual = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.OC3_OperatingCostsTotalAnnual_rule)
        
        self.model.OC4_DiscountedOperatingCostsTotalAnnual = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.OC4_DiscountedOperatingCostsTotalAnnual_rule)
        
        
        #########       	Total Discounted Costs	 	#############
        
        
        self.model.TDC1_TotalDiscountedCostByTechnology = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.TDC1_TotalDiscountedCostByTechnology_rule)
        
        self.model.TDC2_TotalDiscountedCost = Constraint(self.model.REGION, self.model.YEAR, rule=self.TDC2_TotalDiscountedCost_rule)
        
        #self.model.TotalDiscountedCost = Constraint(self.model.REGION, self.model.YEAR, rule=self.TotalDiscountedCost_rule)
        
        
        #########      		Total Capacity Constraints 	##############
        
        self.model.TCC1_TotalAnnualMaxCapacityConstraint = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.TCC1_TotalAnnualMaxCapacityConstraint_rule)
        
        self.model.TCC2_TotalAnnualMinCapacityConstraint = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.TCC2_TotalAnnualMinCapacityConstraint_rule)           
        										 
        
        #########    		New Capacity Constraints  	##############
        
        self.model.NCC1_TotalAnnualMaxNewCapacityConstraint = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.NCC1_TotalAnnualMaxNewCapacityConstraint_rule)
        
        self.model.NCC2_TotalAnnualMinNewCapacityConstraint = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.NCC2_TotalAnnualMinNewCapacityConstraint_rule)       
        
        
        #########   		Annual Activity Constraints	##############
        
        self.model.AAC1_TotalAnnualTechnologyActivity = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.AAC1_TotalAnnualTechnologyActivity_rule)
        
        self.model.AAC2_TotalAnnualTechnologyActivityUpperlimit = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.AAC2_TotalAnnualTechnologyActivityUpperLimit_rule)
        
        self.model.AAC3_TotalAnnualTechnologyActivityLowerlimit = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.AAC3_TotalAnnualTechnologyActivityLowerLimit_rule)
        
        
        #########    		Total Activity Constraints 	##############
        
        self.model.TAC1_TotalModelHorizonTechnologyActivity = Constraint(self.model.REGION, self.model.TECHNOLOGY, rule=self.TAC1_TotalModelHorizonTechnologyActivity_rule)
        
        self.model.TAC2_TotalModelHorizonTechnologyActivityUpperLimit = Constraint(self.model.REGION, self.model.TECHNOLOGY, rule=self.TAC2_TotalModelHorizonTechnologyActivityUpperLimit_rule)
        
        self.model.TAC3_TotalModelHorizonTechnologyActivityLowerLimit = Constraint(self.model.REGION, self.model.TECHNOLOGY, rule=self.TAC3_TotalModelHorizonTechnologyActivityLowerLimit_rule)
        
        
        #########   		Reserve Margin Constraint	############## NTS: Should change demand for production
        
        self.RM1_ReserveMargin_TechnologiesIncluded = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.YEAR, rule=self.RM1_ReserveMargin_TechnologiesIncluded_rule)
        
        self.RM2_ReserveMargin_FuelsIncluded = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.YEAR, rule=self.RM2_ReserveMargin_FuelsIncluded_rule)
        
        self.RM3_ReserveMarginConstraint = Constraint(self.model.REGION, self.model.TIMESLICE, self.model.YEAR, rule=self.RM3_ReserveMarginConstraint_rule)
        
        
            #########   		RE Production Target		############## NTS: Should change demand for production
        
        self.model.RE1_FuelProductionByTechnologyAnnual = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.FUEL, self.model.YEAR, rule=self.RE1_FuelProductionByTechnologyAnnual_rule)
        
        self.model.RE2_TechIncluded = Constraint(self.model.REGION, self.model.YEAR, rule=self.RE2_TechIncluded_rule)
        
        self.model.RE3_FuelIncluded = Constraint(self.model.REGION, self.model.YEAR, rule=self.RE3_FuelIncluded_rule)
        
        self.model.RE4_EnergyConstraint = Constraint(self.model.REGION, self.model.YEAR, rule=self.RE4_EnergyConstraint_rule)
        
        self.model.RE5_FuelUseByTechnologyAnnual = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.FUEL, self.model.YEAR, rule=self.RE5_FuelUseByTechnologyAnnual_rule)
        
        
        #########   		Emissions Accounting		##############
        
        
        self.model.E1_AnnualEmissionProductionByMode = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.EMISSION, self.model.MODE_OF_OPERATION, self.model.YEAR, rule=self.E1_AnnualEmissionProductionByMode_rule)
        
        self.model.E2_AnnualEmissionProduction = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.EMISSION, self.model.YEAR, rule=self.E2_AnnualEmissionProduction_rule)
        
        self.model.E3_EmissionPenaltyByTechAndEmission = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.EMISSION, self.model.YEAR, rule=self.E3_EmissionPenaltyByTechAndEmission_rule)
        
        self.model.E4_EmissionsPenaltyByTechnology = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.E4_EmissionsPenaltyByTechnology_rule)
        
        self.model.E5_DiscountedEmissionsPenaltyByTechnology = Constraint(self.model.REGION, self.model.TECHNOLOGY, self.model.YEAR, rule=self.E5_DiscountedEmissionsPenaltyByTechnology_rule)
        
        self.model.E6_EmissionsAccounting1 = Constraint(self.model.REGION, self.model.EMISSION, self.model.YEAR, rule=self.E6_EmissionsAccounting1_rule)
        
        self.model.E7_EmissionsAccounting2 = Constraint(self.model.REGION, self.model.EMISSION, rule=self.E7_EmissionsAccounting2_rule)
        
        self.model.E8_AnnualEmissionsLimit = Constraint(self.model.REGION, self.model.EMISSION, self.model.YEAR, rule=self.E8_AnnualEmissionsLimit_rule)
        
        self.model.E9_ModelPeriodEmissionsLimit = Constraint(self.model.REGION, self.model.EMISSION, rule=self.E9_ModelPeriodEmissionsLimit_rule)
        
        
    ###########
    # METHODS #
    ###########
    
    
    ######################
    # Objective Function #
    ######################
    
    
    def ObjectiveFunction_rule(self, model):
    	return sum(self.model.ModelPeriodCostByRegion[r] for r in self.model.REGION)
    
    
    ###############
    # Constraints #
    ###############
    
    
    def EQ_SpecifiedDemand_rule(self, model,r,f,l,y):
    	return self.model.SpecifiedAnnualDemand[r,f,y]*self.model.SpecifiedDemandProfile[r,f,l,y]/self.model.YearSplit[l,y] == self.model.RateOfDemand[r,l,f,y]
    
    
    #########       	Capacity Adequacy A	     	#############
    
    	
    def CAa1_TotalNewCapacity_1_rule(self, model,r,t,y):
    	return self.model.AccumulatedNewCapacity[r,t,y] == sum(self.model.NewCapacity[r,t,yy] for yy in self.model.YEAR if ((y-yy < self.model.OperationalLife[r,t]) and (y-yy >= 0)))
    	
    def CAa2_TotalAnnualCapacity_rule(self, model,r,t,y):
    	return self.model.AccumulatedNewCapacity[r,t,y] + self.model.ResidualCapacity[r,t,y] == self.model.TotalCapacityAnnual[r,t,y]
    
    def CAa3_TotalActivityOfEachTechnology_rule(self, model,r,t,l,y):
    	return sum(self.model.RateOfActivity[r,l,t,m,y] for m in self.model.MODE_OF_OPERATION) == self.model.RateOfTotalActivity[r,t,l,y]
    
    def CAa4_ConstraintCapacity_rule(self, model,r,l,t,y):
    	return self.model.RateOfTotalActivity[r,t,l,y] <= self.model.TotalCapacityAnnual[r,t,y]*self.model.CapacityFactor[r,t,l,y]*self.model.CapacityToActivityUnit[r,t]
    
#    def CAa5_TotalNewCapacity_rule(self, model,r,t,y):
#    	if self.model.CapacityOfOneTechnologyUnit != 0:
#    		return self.model.CapacityOfOneTechnologyUnit[r,t,y]*self.model.NumberOfNewTechnologyUnits[r,t,y] == self.model.NewCapacity[r,t,y]
#    	else: 
#    		Constraint.Skip
    
    #########       	Capacity Adequacy B		 	#############
    
    
    def CAb1_PlannedMaintenance_rule(self, model,r,t,y):
    	return sum(self.model.RateOfTotalActivity[r,t,l,y]*self.model.YearSplit[l,y] for l in self.model.TIMESLICE) <= sum(self.model.TotalCapacityAnnual[r,t,y]*self.model.CapacityFactor[r,t,l,y]*self.model.YearSplit[l,y] for l in self.model.TIMESLICE)*self.model.AvailabilityFactor[r,t,y]*self.model.CapacityToActivityUnit[r,t]
    
    
    #########	        Energy Balance A    	 	#############
    
    
    def EBa1_RateOfFuelProduction1_rule(self, model,r,l,f,t,m,y):
    	if self.model.OutputActivityRatio[r,t,f,m,y] != 0:
    		return self.model.RateOfProductionByTechnologyByMode[r,l,t,m,f,y] == self.model.RateOfActivity[r,l,t,m,y]*self.model.OutputActivityRatio[r,t,f,m,y]
    	else:
    		return self.model.RateOfProductionByTechnologyByMode[r,l,t,m,f,y] == 0
        
    def EBa2_RateOfFuelProduction2_rule(self, model,r,l,f,t,y):
    	return  self.model.RateOfProductionByTechnology[r,l,t,f,y] == sum(self.model.RateOfProductionByTechnologyByMode[r,l,t,m,f,y] for m in self.model.MODE_OF_OPERATION)
    
    def EBa3_RateOfFuelProduction3_rule(self, model,r,l,f,y):
    	return self.model.RateOfProduction[r,l,f,y] == sum(self.model.RateOfProductionByTechnology[r,l,t,f,y] for t in self.model.TECHNOLOGY)
    
    def EBa4_RateOfFuelUse1_rule(self, model,r,l,f,t,m,y):
    	if self.model.InputActivityRatio[r,t,f,m,y] != 0:
    		return self.model.RateOfActivity[r,l,t,m,y]*self.model.InputActivityRatio[r,t,f,m,y] == self.model.RateOfUseByTechnologyByMode[r,l,t,m,f,y]
    	else:
    		return self.model.RateOfUseByTechnologyByMode[r,l,t,m,f,y] == 0
    
    def EBa5_RateOfFuelUse2_rule(self, model,r,l,f,t,y):
    	return self.model.RateOfUseByTechnology[r,l,t,f,y] == sum(self.model.RateOfUseByTechnologyByMode[r,l,t,m,f,y] for m in self.model.MODE_OF_OPERATION)
    
    def EBa6_RateOfFuelUse3_rule(self, model,r,l,f,y):
    	return sum(self.model.RateOfUseByTechnology[r,l,t,f,y] for t in self.model.TECHNOLOGY) == self.model.RateOfUse[r,l,f,y]
    
    def EBa7_EnergyBalanceEachTS1_rule(self, model,r,l,f,y):
    	return self.model.RateOfProduction[r,l,f,y]*self.model.YearSplit[l,y] == self.model.Production[r,l,f,y]
    
    def EBa8_EnergyBalanceEachTS2_rule(self, model,r,l,f,y):
    	return self.model.RateOfUse[r,l,f,y]*self.model.YearSplit[l,y] == self.model.Use[r,l,f,y]
    
    def EBa9_EnergyBalanceEachTS3_rule(self, model,r,l,f,y):
    	return self.model.RateOfDemand[r,l,f,y]*self.model.YearSplit[l,y] == self.model.Demand[r,l,f,y]
    
    def EBa10_EnergyBalanceEachTS4_rule(self, model,r,rr,l,f,y):
    	return self.model.Trade[r,rr,l,f,y] + self.model.Trade[rr,r,l,f,y] == 0
    
    def EBa11_EnergyBalanceEachTS5_rule(self, model,r,l,f,y):
    	return self.model.Production[r,l,f,y] >= self.model.Demand[r,l,f,y] + self.model.Use[r,l,f,y] + sum(self.model.Trade[r,rr,l,f,y]*self.model.TradeRoute[r,rr,f,y] for rr in self.model.REGION)
    
    
    #########        	Energy Balance B		 	#############
    
    
    def EBb1_EnergyBalanceEachYear1_rule(self, model,r,f,y):
    	return sum(self.model.Production[r,l,f,y] for l in self.model.TIMESLICE) == self.model.ProductionAnnual[r,f,y]
    
    def EBb2_EnergyBalanceEachYear2_rule(self, model,r,f,y):
    	return sum(self.model.Use[r,l,f,y] for l in self.model.TIMESLICE) == self.model.UseAnnual[r,f,y]
    
    def EBb3_EnergyBalanceEachYear3_rule(self, model,r,rr,f,y):
    	return sum(self.model.Trade[r,rr,l,f,y] for l in self.model.TIMESLICE) == self.model.TradeAnnual[r,rr,f,y]
    	
    def EBb4_EnergyBalanceEachYear4_rule(self, model,r,f,y):
    	return self.model.ProductionAnnual[r,f,y] >= self.model.UseAnnual[r,f,y] + sum(self.model.TradeAnnual[r,rr,f,y]*self.model.TradeRoute[r,rr,f,y] for rr in self.model.REGION) + self.model.AccumulatedAnnualDemand[r,f,y]
    
    
    #########        	Accounting Technology Production/Use	#############
    
    	
    def Acc1_FuelProductionByTechnology_rule(self, model,r,l,t,f,y):
    	return self.model.RateOfProductionByTechnology[r,l,t,f,y]*self.model.YearSplit[l,y] == self.model.ProductionByTechnology[r,l,t,f,y]
    
    def Acc2_FuelUseByTechnology_rule(self, model,r,l,t,f,y):
    	return self.model.RateOfUseByTechnology[r,l,t,f,y]*self.model.YearSplit[l,y] == self.model.UseByTechnology[r,l,t,f,y]
    
    def Acc3_AverageAnnualRateOfActivity_rule(self, model,r,t,m,y):
    	return sum(self.model.RateOfActivity[r,l,t,m,y]*self.model.YearSplit[l,y] for l in self.model.TIMESLICE) == self.model.TotalAnnualTechnologyActivityByMode[r,t,m,y]
    
    def Acc4_ModelPeriodCostByRegion_rule(self, model,r):
    	return self.model.ModelPeriodCostByRegion[r] == sum(self.model.TotalDiscountedCost[r,y] for y in self.model.YEAR)
    
    # def Acc5_ModelPeriodCost_rule(self, model):
    	# return self.model.ModelPeriodCost == sum(self.model.ModelPeriodCostByRegion[r] for r in self.model.REGION)
    
    
    #########        	Storage Equations			#############
    
    
    def S1_RateOfStorageCharge_rule(self, model,r,s,ls,ld,lh,y):
        return sum(self.model.RateOfActivity[r,l,t,m,y] * self.model.TechnologyToStorage[r,t,s,m] * self.model.Conversionls[l,ls] * self.model.Conversionld[l,ld] * self.model.Conversionlh[l,lh] for t in self.model.TECHNOLOGY for m in self.model.MODE_OF_OPERATION for l in self.model.TIMESLICE if self.model.TechnologyToStorage[r,t,s,m]>0) == self.model.RateOfStorageCharge[r,s,ls,ld,lh,y]
        #return sum(sum(sum(self.model.RateOfActivity[r,l,t,m,y] * self.model.TechnologyToStorage[r,t,s,m] * self.model.Conversionls[l,ls] * self.model.Conversionld[l,ld] * self.model.Conversionlh[l,lh] for l in self.model.TIMESLICE if self.model.TechnologyToStorage[r,t,s,m]>0) for m in self.model.MODE_OF_OPERATION) for t in self.model.TECHNOLOGY) == self.model.RateOfStorageCharge[r,s,ls,ld,lh,y]
        
    def S2_RateOfStorageDischarge_rule(self, model,r,s,ls,ld,lh,y):
        return sum(self.model.RateOfActivity[r,l,t,m,y] * self.model.TechnologyFromStorage[r,t,s,m] * self.model.Conversionls[l,ls] * self.model.Conversionld[l,ld] * self.model.Conversionlh[l,lh] for t in self.model.TECHNOLOGY for m in self.model.MODE_OF_OPERATION for l in self.model.TIMESLICE if self.model.TechnologyFromStorage[r,t,s,m]>0) == self.model.RateOfStorageDischarge[r,s,ls,ld,lh,y]
    
    def S3_NetChargeWithinYear_rule(self, model,r,s,ls,ld,lh,y):
        return sum((self.model.RateOfStorageCharge[r,s,ls,ld,lh,y] - self.model.RateOfStorageDischarge[r,s,ls,ld,lh,y]) * self.model.YearSplit[l,y] * self.model.Conversionls[l,ls] * self.model.Conversionld[l,ld] * self.model.Conversionlh[l,lh] for l in self.model.TIMESLICE if (self.model.Conversionls[l,ls]>0 and self.model.Conversionld[l,ld]>0 and self.model.Conversionlh[l,lh]>0)) == self.model.NetChargeWithinYear[r,s,ls,ld,lh,y]
    
    def S4_NetChargeWithinDay_rule(self, model,r,s,ls,ld,lh,y):
        return (self.model.RateOfStorageCharge[r,s,ls,ld,lh,y] - self.model.RateOfStorageDischarge[r,s,ls,ld,lh,y]) * self.model.DaySplit[lh,y] == self.model.NetChargeWithinDay[r,s,ls,ld,lh,y]
        
    def S5_and_S6_StorageLevelYearStart_rule(self, model,r,s,y):
        if y == min(yy for yy in self.model.YEAR):
            return self.model.StorageLevelStart[r,s] == self.model.StorageLevelYearStart[r,s,y]
        else:
            return self.model.StorageLevelYearStart[r,s,y-1] + sum(self.model.NetChargeWithinYear[r,s,ls,ld,lh,y-1] for ls in self.model.SEASON for ld in self.model.DAYTYPE for lh in self.model.DAILYTIMEBRACKET) == self.model.StorageLevelYearStart[r,s,y]
    
    def S7_and_S8_StorageLevelYearFinish_rule(self, model,r,s,y):
        if y < max(yy for yy in self.model.YEAR):
            return self.model.StorageLevelYearStart[r,s,y+1] == self.model.StorageLevelYearFinish[r,s,y]
        else:
            return self.model.StorageLevelYearStart[r,s,y] + sum(self.model.NetChargeWithinYear[r,s,ls,ld,lh,y] for ls in self.model.SEASON for ld in self.model.DAYTYPE for lh in self.model.DAILYTIMEBRACKET) == self.model.StorageLevelYearFinish[r,s,y]
    
    def S9_and_S10_StorageLevelSeasonStart_rule(self, model,r,s,ls,y):
        if ls == min(lsls for lsls in self.model.SEASON):
            return self.model.StorageLevelYearStart[r,s,y] == self.model.StorageLevelSeasonStart[r,s,ls,y]
        else:
            return self.model.StorageLevelSeasonStart[r,s,ls-1,y] + sum(self.model.NetChargeWithinYear[r,s,ls-1,ld,lh,y] for ld in self.model.DAYTYPE for lh in self.model.DAILYTIMEBRACKET) == self.model.StorageLevelSeasonStart[r,s,ls,y]
    
    def S11_and_S12_StorageLevelDayTypeStart_rule(self, model,r,s,ls,ld,y):
        if ld == min(ldld for ldld in self.model.DAYTYPE):
            return self.model.StorageLevelSeasonStart[r,s,ls,y] == self.model.StorageLevelDayTypeStart[r,s,ls,ld,y]
        else:
            return self.model.StorageLevelDayTypeStart[r,s,ls,ld-1,y] + sum(self.model.NetChargeWithinDay[r,s,ls,ld-1,lh,y] * self.model.DaysInDayType[ls,ld-1,y] for lh in self.model.DAILYTIMEBRACKET) == self.model.StorageLevelDayTypeStart[r,s,ls,ld,y]
    
    def S13_and_S14_and_S15_StorageLevelDayTypeFinish_rule(self, model,r,s,ls,ld,y):
        if ls == max(lsls for lsls in self.model.SEASON) and ld == max(ldld for ldld in self.model.DAYTYPE):
            return self.model.StorageLevelYearFinish[r,s,y] == self.model.StorageLevelDayTypeFinish[r,s,ls,ld,y]
        elif ld == max(ldld for ldld in self.model.DAYTYPE):
            return self.model.StorageLevelSeasonStart[r,s,ls+1,y] == self.model.StorageLevelDayTypeFinish[r,s,ls,ld,y]
        else:
            return self.model.StorageLevelDayTypeFinish[r,s,ls,ld+1,y] - sum(self.model.NetChargeWithinDay[r,s,ls,ld+1,lh,y] * self.model.DaysInDayType[ls,ld+1,y] for lh in self.model.DAILYTIMEBRACKET) == self.model.StorageLevelDayTypeFinish[r,s,ls,ld,y]
    
    
    ##########		Storage Constraints				#############
    
    
    def SC1_LowerLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInFirstWeekConstraint_rule(self, model,r,s,ls,ld,lh,y):
        return 0 <= (self.model.StorageLevelDayTypeStart[r,s,ls,ld,y] + sum(self.model.NetChargeWithinDay[r,s,ls,ld,lhlh,y] for lhlh in self.model.DAILYTIMEBRACKET if lh-lhlh>0)) - self.model.StorageLowerLimit[r,s,y]
    
    def SC1_UpperLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInFirstWeekConstraint_rule(self, model,r,s,ls,ld,lh,y):
        return (self.model.StorageLevelDayTypeStart[r,s,ls,ld,y] + sum(self.model.NetChargeWithinDay[r,s,ls,ld,lhlh,y] for lhlh in self.model.DAILYTIMEBRACKET if lh-lhlh>0)) - self.model.StorageUpperLimit[r,s,y] <= 0
    
    def SC2_LowerLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInFirstWeekConstraint_rule(self, model, r,s,ls,ld,lh,y):
        if ld > min(ldld for ldld in self.model.DAYTYPE):
            return 0 <= (self.model.StorageLevelDayTypeStart[r,s,ls,ld,y] - sum(self.model.NetChargeWithinDay[r,s,ls,ld-1,lhlh,y] for lhlh in self.model.DAILYTIMEBRACKET if lh-lhlh<0)) - self.model.StorageLowerLimit[r,s,y]
        else:
            return Constraint.Skip
    
    def SC2_UpperLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInFirstWeekConstraint_rule(self, model,r,s,ls,ld,lh,y):
        if ld > min(ldld for ldld in self.model.DAYTYPE):
            return (self.model.StorageLevelDayTypeStart[r,s,ls,ld,y] - sum(self.model.NetChargeWithinDay[r,s,ls,ld-1,lhlh,y] for lhlh in self.model.DAILYTIMEBRACKET if lh-lhlh<0)) - self.model.StorageUpperLimit[r,s,y] <= 0
        else:
            return Constraint.Skip
    
    def SC3_LowerLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInLastWeekConstraint_rule(self, model,r,s,ls,ld,lh,y):
        return 0 <= (self.model.StorageLevelDayTypeFinish[r,s,ls,ld,y] - sum(self.model.NetChargeWithinDay[r,s,ls,ld,lhlh,y] for lhlh in self.model.DAILYTIMEBRACKET if lh-lhlh<0)) - self.model.StorageLowerLimit[r,s,y]
    
    def SC3_UpperLimit_EndOfDailyTimeBracketOfLastInstanceOfDayTypeInLastWeekConstraint_rule(self, model, r,s,ls,ld,lh,y):
        return (self.model.StorageLevelDayTypeFinish[r,s,ls,ld,y] - sum(self.model.NetChargeWithinDay[r,s,ls,ld,lhlh,y] for lhlh in self.model.DAILYTIMEBRACKET if lh-lhlh<0)) - self.model.StorageUpperLimit[r,s,y] <= 0
    
    def SC4_LowerLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInLastWeekConstraint_rule(self, model,r,s,ls,ld,lh,y):
        if ld > min(ldld for ldld in self.model.DAYTYPE):
            return 0 <= (self.model.StorageLevelDayTypeFinish[r,s,ls,ld-1,y] + sum(self.model.NetChargeWithinDay[r,s,ls,ld,lhlh,y] for lhlh in self.model.DAILYTIMEBRACKET if lh-lhlh>0)) - self.model.StorageLowerLimit[r,s,y]
        else:
            return Constraint.Skip
    
    def SC4_UpperLimit_BeginningOfDailyTimeBracketOfFirstInstanceOfDayTypeInLastWeekConstraint_rule(self, model, r,s,ls,ld,lh,y):
        if ld > min(ldld for ldld in self.model.DAYTYPE):
            return (self.model.StorageLevelDayTypeFinish[r,s,ls,ld-1,y] + sum(self.model.NetChargeWithinDay[r,s,ls,ld,lhlh,y] for lhlh in self.model.DAILYTIMEBRACKET if lh-lhlh>0)) - self.model.StorageUpperLimit[r,s,y] <= 0
        else:
            return Constraint.Skip
    
    def SC5_MaxChargeConstraint_rule(self, model,r,s,ls,ld,lh,y):
        return self.model.RateOfStorageCharge[r,s,ls,ld,lh,y] <= self.model.StorageMaxChargeRate[r,s]
    
    def SC6_MaxDischargeConstraint_rule(self, model,r,s,ls,ld,lh,y):
        return self.model.RateOfStorageDischarge[r,s,ls,ld,lh,y] <= self.model.StorageMaxDischargeRate[r,s]
    
    
    #########		Storage Investments				#############
    
    
    def SI1_StorageUpperLimit_rule(self, model,r,s,y):
        return self.model.AccumulatedNewStorageCapacity[r,s,y] + self.model.ResidualStorageCapacity[r,s,y] == self.model.StorageUpperLimit[r,s,y]
    
    def SI2_StorageLowerLimit_rule(self, model,r,s,y):
        return self.model.MinStorageCharge[r,s,y] * self.model.StorageUpperLimit[r,s,y] == self.model.StorageLowerLimit[r,s,y]
    
    def SI3_TotalNewStorage_rule(self, model,r,s,y):
        return sum(self.model.NewStorageCapacity[r,s,yy] for yy in self.model.YEAR if y-yy < self.model.OperationalLifeStorage[r,s] and y-yy>=0) == self.model.AccumulatedNewStorageCapacity[r,s,y]
    
    def SI4_UndiscountedCapitalInvestmentStorage_rule(self, model,r,s,y):
        return self.model.CapitalCostStorage[r,s,y] * self.model.NewStorageCapacity[r,s,y] == self.model.CapitalInvestmentStorage[r,s,y]
    
    def SI5_DiscountingCapitalInvestmentStorage_rule(self, model,r,s,y):
        return self.model.CapitalInvestmentStorage[r,s,y] / ((1 + self.model.DiscountRate[r])**(y - min(yy for yy in self.model.YEAR))) == self.model.DiscountedCapitalInvestmentStorage[r,s,y]
    
    def SI6_SalvageValueStorageAtEndOfPeriod1_rule(self, model,r,s,y):
        if (y + self.model.OperationalLifeStorage[r,s] - 1) <= max(yy for yy in self.model.YEAR):
            return 0 == self.model.SalvageValueStorage[r,s,y]
        else:
            return Constraint.Skip
    
    def SI7_SalvageValueStorageAtEndOfPeriod2_rule(self, model,r,s,y):
        if ((self.model.DepreciationMethod[r]==1 and (y + self.model.OperationalLifeStorage[r,s] - 1) > max(yy for yy in self.model.YEAR) and self.model.DiscountRate[r]==0) or (self.model.DepreciationMethod[r]==2 and (y + self.model.OperationalLifeStorage[r,s] - 1) > max(yy for yy in self.model.YEAR))):
            return self.model.CapitalInvestmentStorage[r,s,y] * (1 - (max(yy for yy in self.model.YEAR) - y+1) / self.model.OperationalLifeStorage[r,s]) == self.model.SalvageValueStorage[r,s,y]
        else:
            return Constraint.Skip
    
    def SI8_SalvageValueStorageAtEndOfPeriod3_rule(self, model,r,s,y):
        if self.model.DepreciationMethod[r]==1 and (y + self.model.OperationalLifeStorage[r,s] - 1) > max(yy for yy in self.model.YEAR) and self.model.DiscountRate[r]>0:
            return self.model.CapitalInvestmentStorage[r,s,y] * (1- (((1 + self.model.DiscountRate[r])**(max(yy for yy in self.model.YEAR) - y+1)-1) / ((1 + self.model.DiscountRate[r])**self.model.OperationalLifeStorage[r,s] - 1))) == self.model.SalvageValueStorage[r,s,y]
        else:
            return Constraint.Skip
    
    def SI9_SalvageValueStorageDiscountedToStartYear_rule(self, model,r,s,y):
        return self.model.SalvageValueStorage[r,s,y] / ((1 + self.model.DiscountRate[r])**(max(yy for yy in self.model.YEAR) - min(yy for yy in self.model.YEAR) + 1)) == self.model.DiscountedSalvageValueStorage[r,s,y]
    
    def SI10_TotalDiscountedCostByStorage_rule(self, model,r,s,y):
        return self.model.DiscountedCapitalInvestmentStorage[r,s,y] - self.model.DiscountedSalvageValueStorage[r,s,y] == self.model.TotalDiscountedStorageCost[r,s,y]
    
    
    #########       	Capital Costs 		     	#############
    
    
    def CC1_UndiscountedCapitalInvestment_rule(self, model,r,t,y):
    	return self.model.CapitalCost[r,t,y]*self.model.NewCapacity[r,t,y] == self.model.CapitalInvestment[r,t,y]
    
    def CC2_DiscountedCapitalInvestment_rule(self, model,r,t,y):
    	return self.model.CapitalInvestment[r,t,y]/((1+self.model.DiscountRate[r])**(y-min(self.model.YEAR))) == self.model.DiscountedCapitalInvestment[r,t,y]
    
    
    #########           Salvage Value            	#############
    
    def SV1_SalvageValueAtEndOfPeriod1_rule(self, model,r,t,y): 
    	if self.model.DepreciationMethod[r] == 1 and ((y + self.model.OperationalLife[r,t]-1) > max(self.model.YEAR)) and self.model.DiscountRate[r]>0: 
    		return self.model.SalvageValue[r,t,y] == self.model.CapitalCost[r,t,y]*self.model.NewCapacity[r,t,y]*(1-(((1+self.model.DiscountRate[r])**(max(self.model.YEAR)- y+1)-1)/((1+self.model.DiscountRate[r])**self.model.OperationalLife[r,t]-1)))
    	elif (self.model.DepreciationMethod[r] == 1 and ((y + self.model.OperationalLife[r,t]-1) > max(self.model.YEAR)) and self.model.DiscountRate[r] == 0) or (self.model.DepreciationMethod[r] == 2 and (y + self.model.OperationalLife[r,t]-1) > (max(self.model.YEAR))):
    		return self.model.SalvageValue[r,t,y] == self.model.CapitalCost[r,t,y]*self.model.NewCapacity[r,t,y]*(1-(max(self.model.YEAR)- y+1)/self.model.OperationalLife[r,t])
    	else:
    		return self.model.SalvageValue[r,t,y] == 0
    
    def SV4_SalvageValueDiscountedToStartYear_rule(self, model,r,t,y):
    	return self.model.DiscountedSalvageValue[r,t,y] == self.model.SalvageValue[r,t,y]/((1+self.model.DiscountRate[r])**(1+max(self.model.YEAR)-min(self.model.YEAR)))
    
    
    #########        	Operating Costs 		 	#############
    
    
    def OC1_OperatingCostsVariable_rule(self, model,r,t,l,y):
    	return sum(self.model.TotalAnnualTechnologyActivityByMode[r,t,m,y]*self.model.VariableCost[r,t,m,y] for m in self.model.MODE_OF_OPERATION) == self.model.AnnualVariableOperatingCost[r,t,y]
    
    def OC2_OperatingCostsFixedAnnual_rule(self, model,r,t,y):
    	return self.model.TotalCapacityAnnual[r,t,y]*self.model.FixedCost[r,t,y] == self.model.AnnualFixedOperatingCost[r,t,y]
    
    def OC3_OperatingCostsTotalAnnual_rule(self, model,r,t,y):
    	return self.model.AnnualFixedOperatingCost[r,t,y] + self.model.AnnualVariableOperatingCost[r,t,y] == self.model.OperatingCost[r,t,y]
    
    def OC4_DiscountedOperatingCostsTotalAnnual_rule(self, model,r,t,y):
    	return self.model.OperatingCost[r,t,y]/((1+self.model.DiscountRate[r])**(y-min(self.model.YEAR) + 0.5)) == self.model.DiscountedOperatingCost[r,t,y]
    
    
    #########       	Total Discounted Costs	 	#############
    
    
    def TDC1_TotalDiscountedCostByTechnology_rule(self, model,r,t,y):
    	return self.model.DiscountedOperatingCost[r,t,y] + self.model.DiscountedCapitalInvestment[r,t,y] + self.model.DiscountedTechnologyEmissionsPenalty[r,t,y] - self.model.DiscountedSalvageValue[r,t,y] == self.model.TotalDiscountedCostByTechnology[r,t,y]
    
    def TDC2_TotalDiscountedCost_rule(self, model,r,y):
    	return sum(self.model.TotalDiscountedCostByTechnology[r,t,y] for t in self.model.TECHNOLOGY) + sum(self.model.TotalDiscountedStorageCost[r,s,y] for s in self.model.STORAGE) == self.model.TotalDiscountedCost[r,y]
    
    #def TDC2_TotalDiscountedCost_rule(self, model,r,y):
    #	return sum(self.model.TotalDiscountedCostByTechnology[r,t,y] for t in self.model.TECHNOLOGY) == self.model.TotalDiscountedCost[r,y]
    
    
    #########      		Total Capacity Constraints 	##############
    
    def TCC1_TotalAnnualMaxCapacityConstraint_rule(self, model,r,t,y): 
    	return self.model.TotalCapacityAnnual[r,t,y] <= self.model.TotalAnnualMaxCapacity[r,t,y]
    
    def TCC2_TotalAnnualMinCapacityConstraint_rule(self, model,r,t,y): 
    	return self.model.TotalCapacityAnnual[r,t,y] >= self.model.TotalAnnualMinCapacity[r,t,y]
    										 
    
    #########    		New Capacity Constraints  	##############
    
    def NCC1_TotalAnnualMaxNewCapacityConstraint_rule(self, model,r,t,y): 
    	return self.model.NewCapacity[r,t,y] <= self.model.TotalAnnualMaxCapacityInvestment[r,t,y]
    
    def NCC2_TotalAnnualMinNewCapacityConstraint_rule(self, model,r,t,y): 
    	return self.model.NewCapacity[r,t,y] >= self.model.TotalAnnualMinCapacityInvestment[r,t,y]
    
    
    #########   		Annual Activity Constraints	##############
    
    def AAC1_TotalAnnualTechnologyActivity_rule(self, model,r,t,y):
    	return sum(self.model.RateOfTotalActivity[r,t,l,y]*self.model.YearSplit[l,y] for l in self.model.TIMESLICE) == self.model.TotalTechnologyAnnualActivity[r,t,y]
    
    def AAC2_TotalAnnualTechnologyActivityUpperLimit_rule(self, model,r,t,y):
    	return self.model.TotalTechnologyAnnualActivity[r,t,y] <= self.model.TotalTechnologyAnnualActivityUpperLimit[r,t,y]
    
    def AAC3_TotalAnnualTechnologyActivityLowerLimit_rule(self, model,r,t,y):
    	return self.model.TotalTechnologyAnnualActivity[r,t,y] >= self.model.TotalTechnologyAnnualActivityLowerLimit[r,t,y]
    
    
    #########    		Total Activity Constraints 	##############
    
    def TAC1_TotalModelHorizonTechnologyActivity_rule(self, model,r,t):
    	return sum(self.model.TotalTechnologyAnnualActivity[r,t,y] for y in self.model.YEAR) == self.model.TotalTechnologyModelPeriodActivity[r,t]
    
    def TAC2_TotalModelHorizonTechnologyActivityUpperLimit_rule(self, model,r,t):
    	return self.model.TotalTechnologyModelPeriodActivity[r,t] <= self.model.TotalTechnologyModelPeriodActivityUpperLimit[r,t]
    
    def TAC3_TotalModelHorizonTechnologyActivityLowerLimit_rule(self, model,r,t):
    	return self.model.TotalTechnologyModelPeriodActivity[r,t] >= self.model.TotalTechnologyModelPeriodActivityLowerLimit[r,t]
    
    
        #########   		Reserve Margin Constraint	############## NTS: Should change demand for production
    
    def RM1_ReserveMargin_TechnologiesIncluded_rule(self, model,r,l,y):
    	return (sum((self.model.TotalAnnualCapacity[r,t,y]*self.model.ReserveMarginTagTechnology[r,t,y]*self.model.CapacityToActivityUnit[r,t]) for t in self.model.TECHNOLOGY) == self.model.TotalCapacityInReserveMargin[r,y])
    
    def RM2_ReserveMargin_FuelsIncluded_rule(self, model,r,l,y):
    	return sum((self.model.RateOfProduction[r,l,f,y]*self.model.ReserveMarginTagFuel) for f in self.model.FUEL) == self.model.DemandNeedingReserveMargin[r,l,y]
    
    def RM3_ReserveMarginConstraint_rule(self, model,r,l,y):
    	return self.model.DemandNeedingReserveMargin[r,l,y]*self.model.ReserveMargin[r,y] <= self.model.TotalCapacityInReserveMargin[r,y]
    
    
    #########   		RE Production Target		############## NTS: Should change demand for production
    
    def RE1_FuelProductionByTechnologyAnnual_rule(self, model,r,t,f,y):
        return sum(self.model.ProductionByTechnology[r,l,t,f,y] for l in self.model.TIMESLICE) == self.model.ProductionByTechnologyAnnual[r,t,f,y]
    
    def RE2_TechIncluded_rule(self, model,r,y):
        return sum(self.model.ProductionByTechnologyAnnual[r,t,f,y] * self.model.RETagTechnology[r,t,y] for t in self.model.TECHNOLOGY for f in self.model.FUEL)  == self.model.TotalREProductionAnnual[r,y];
    
    def RE3_FuelIncluded_rule(self, model,r,y):
        return sum(self.model.RateOfDemand[r,l,f,y] * self.model.YearSplit[l,y] * self.model.RETagFuel[r,f,y] for l in self.model.TIMESLICE for f in self.model.FUEL) == self.model.RETotalDemandOfTargetFuelAnnual[r,y]
    
    def RE4_EnergyConstraint_rule(self, model,r,y):
        return self.model.REMinProductionTarget[r,y] * self.model.RETotalDemandOfTargetFuelAnnual[r,y] <= self.model.TotalREProductionAnnual[r,y]
    
    def RE5_FuelUseByTechnologyAnnual_rule(self, model,r,t,f,y):
        return sum(self.model.RateOfUseByTechnology[r,l,t,f,y] * self.model.YearSplit[l,y] for l in self.model.TIMESLICE) == self.model.UseByTechnologyAnnual[r,t,f,y]
    
    
    #########   		Emissions Accounting		##############
    
    
    def E1_AnnualEmissionProductionByMode_rule(self, model,r,t,e,m,y):
    	if self.model.EmissionActivityRatio[r,t,e,m,y] != 0:
    		return self.model.EmissionActivityRatio[r,t,e,m,y] * self.model.TotalAnnualTechnologyActivityByMode[r,t,m,y] == self.model.AnnualTechnologyEmissionByMode[r,t,e,m,y]
    	else:
    		return self.model.AnnualTechnologyEmissionByMode[r,t,e,m,y] == 0
    
    def E2_AnnualEmissionProduction_rule(self, model,r,t,e,y):
    	return sum(self.model.AnnualTechnologyEmissionByMode[r,t,e,m,y] for m in self.model.MODE_OF_OPERATION) == self.model.AnnualTechnologyEmission[r,t,e,y]
    
    def E3_EmissionPenaltyByTechAndEmission_rule(self, model,r,t,e,y):
    	return self.model.AnnualTechnologyEmission[r,t,e,y]*self.model.EmissionsPenalty[r,e,y] == self.model.AnnualTechnologyEmissionPenaltyByEmission[r,t,e,y]
    
    def E4_EmissionsPenaltyByTechnology_rule(self, model,r,t,y):
    	return sum(self.model.AnnualTechnologyEmissionPenaltyByEmission[r,t,e,y] for e in self.model.EMISSION) == self.model.AnnualTechnologyEmissionsPenalty[r,t,y]
    
    def E5_DiscountedEmissionsPenaltyByTechnology_rule(self, model,r,t,y):
    	return self.model.AnnualTechnologyEmissionsPenalty[r,t,y]/((1+self.model.DiscountRate[r])**(y-min(self.model.YEAR)+0.5)) == self.model.DiscountedTechnologyEmissionsPenalty[r,t,y]
    
    def E6_EmissionsAccounting1_rule(self, model,r,e,y):
    	return sum(self.model.AnnualTechnologyEmission[r,t,e,y] for t in self.model.TECHNOLOGY) == self.model.AnnualEmissions[r,e,y]
    
    def E7_EmissionsAccounting2_rule(self, model,r,e):
    	return sum(self.model.AnnualEmissions[r,e,y] for y in self.model.YEAR) == self.model.ModelPeriodEmissions[r,e] - self.model.ModelPeriodExogenousEmission[r,e]
    
    def E8_AnnualEmissionsLimit_rule(self, model,r,e,y):
    	return self.model.AnnualEmissions[r,e,y] + self.model.AnnualExogenousEmission[r,e,y] <= self.model.AnnualEmissionLimit[r,e,y]
    
    def E9_ModelPeriodEmissionsLimit_rule(self, model,r,e):
    	return self.model.ModelPeriodEmissions[r,e] <= self.model.ModelPeriodEmissionLimit[r,e]
    
    
    #############################
    # Initialize abstract model #
    #############################
    
    def load_data(self):
        '''
        Loads input data for Sets and Params from csv files into a Pyomo DataPortal.
        
        This method is pretty verbose, that is it checks for each Set and Param of 
        the abstract model if a csv file with the same name and .csv extension exists
        at InputPath. If no such file exists, it warns the user that default values
        will be used.
        
        No csv file at all (incl. no empty file or with header only) should be provided
        for the Sets and Params for which the user wishes to use default values. If you 
        do Pyomo will throw an error indicating that it cannot initialize the 
        corresponding Set or Param.
        
        Arguments:
            None
        Returns:
            None
        '''        
        # SETS
        # Get a dict of the abstract's model Set names (key) and Set objects (value)
        # The condition is to remove the param with e.g. _index_index_0 from the list
        ModelSets = {s.name:s for  s in self.model.component_objects(Set, descend_into=True) if 'index' not in s.name}
        # If there is a csv file at location InputPath with the same name as the Set, load the data
        print('\n####################################')
        print('\nInitializing Sets of abstract model...')
        for set_name, set_object in ModelSets.items():
            if os.path.isfile(os.path.join(self.InputPath, set_name + '.csv')):
                self.data.load(filename=os.path.join(self.InputPath, set_name + '.csv'), set=set_object)
            else:
                print('\nCannot find file <' + os.path.join(self.InputPath, set_name + '.csv') + '>. Using default values instead.')
        
        # PARAMETERS
        # Get a list of the abstract's model param names
        ModelParams = {p.name:p for  p in self.model.component_objects(Param, descend_into=True)}
        # If there is a csv file at location InputPath with the same name as the Param, load the data
        print('\n####################################')
        print('\nInitializing Params of abstract model...')
        for param_name, param_object in ModelParams.items():
            if os.path.isfile(os.path.join(self.InputPath, param_name + '.csv')):
                self.data.load(filename=os.path.join(self.InputPath, param_name + '.csv'), param=param_object)
            else:
                print('\nCannot find file <' + os.path.join(self.InputPath, param_name + '.csv') + '>. Using default values instead.')
    

##############################################################################
##############################################################################           

class concrete_OSeMOSYS(object):
    '''
    Class providing methods to instantiate a concrete model from an abstract OSeMOSYS
    model, solve it and export results using Pyomo.
    
    Constructor arguments:
        OSeMOSYSAbstractModel: abstract_OSeMOSYS object
            Instance of the abstract_OSeMOSYS class.
        InputFile [optional]: string
            Path (incl. file name) to a .dat input data file in AMPL format.
        OutputPath [optional]: string
            Path to directory where all variables ca be saved as csv files after 
            solving the model. Default: None.
        OutputCode [optional]: string
            Short string that will be appended at the beginning of the csv file names
            when exporting variables (useful if one wants to save different model 
            runs in the same output directory). Also used as the name of the ConcreteModel
            instance. Default: None.
        Solver [optional]: string
            Name of the solver that will be passed to the function pyomo.opt.SolverFactory.
            Default: 'glpk'

    Public class attributes:
        instance: Pyomo ConcreteModel object
            Instantiated model built from OSeMOSYSAbstractModel.model AbstractModel object.
            Parameter values are supplied via the AMPL InputFile if provided or via the 
            OSeMOSYSAbstractModel.data DataPortal object (loaded with input data from csv files).
        OptSolver: Pyomo SolverFactory object
            Solver object obtained from pyomo.opt.SolverFactory(Solver) where 'Solver'
            is 'glpk' by default.
        results: Pyomo results object
            Result object returned by OptSolver.solve().
        OutputPath: string
            Path to directory where all variables ca be saved as csv files after 
            solving the model.
        OutputCode: string
            Short string that will be appended at the beginning of the csv file names
            when exporting variables (useful if one wants to save different model 
            runs in the same output directory). Also used as the name of the ConcreteModel
            instance.
    '''
    def __init__(self, OSeMOSYSAbstractModel, InputFile=None, OutputPath=None, OutputCode=None, Solver='glpk'):
                
        print('\n####################################')
        print('\nBuilding a concrete model...')
        
        # Path to directory where to export variables as csv files (optional)
        self.OutputPath = OutputPath
        # Code that will be inserted before the variable name when exporting variables to csv
        # (useful if you want to export results of different model runs to the same directory)
        # It is also used to name concrete model instances
        self.OutputCode = OutputCode
        
        # Select LP solver
        self.OptSolver = SolverFactory(Solver)
        # Create a concrete model instance
        # If an input file (*.dat) is provided, use it, otherwise assume that
        # input data have been loaded in a pyomo DataPortal for the abstract model
        if InputFile is not None:
            self.instance = OSeMOSYSAbstractModel.model.create_instance(InputFile)
            self.instance.name = OutputCode
        else:
            self.instance = OSeMOSYSAbstractModel.model.create_instance(
                OSeMOSYSAbstractModel.data)
            self.instance.name = OutputCode
        
    ###########
    # METHODS #
    ###########
    
    def solve_model(self):
        '''
        Calls the solver on the initialised concrete model.
        
        This method instantiates the public attribute 'results' and prints it out.
        
        Arguments:
            None
        Returns:
            None
        '''
        print('\n####################################')
        if self.instance.name is not None:
            print('\n' + self.instance.name)
        print('\nSolving concrete model...')
        self.results = self.OptSolver.solve(self.instance)
        print('\nResults:')
        print(self.results)
    
    def export_lp_problem(self):
        '''
        Saves the LP problem equations to a text file at the provided OutputPath.
        
        Arguments:
            None
        Returns:
            None
        '''
        self.instance.write(os.path.join(self.OutputPath, 'problem.lp'),
                                         io_options={'symbolic_solver_labels':True})
        
    def export_all_var(self):
        '''
        Exports all variables to csv files in the directory provided
        with OutputPath.
        
        OutputPath should exist, otherwise the function will crash.
        If OutputPath exists and is not empty, existing csv files may be silently overwritten.
        OutputCode, if provided, is appended at the beginning of the csv file names 
        (useful if one wants to save different model runs in the same output directory).
        
        The csv files are structured as follows:
            * header: comma separated names of sets making of the index of 
              the variable + variable name at the end of the row
            * values: comma separated set values + variable value at the end of the row
            
        Examples:
            
            * variable with one set in index:
                
                | REGION,ModelPeriodCostByRegion
                | DE,1243122.6263322
            
            * variable with multiple sets in index:
                
                | REGION,TECHNOLOGY,YEAR,DiscountedCapitalInvestment
                | DE,PP_NG_CC,2048,0.0
                | DE,PP_NG_CC,2049,0.0
                | DE,PP_NG_CC,2050,0.0
                | ...
        
        Arguments:
            None
        Returns:
            None
        '''
        # Export all active variables
        for variable in self.instance.component_objects(Var, active=True):
            
            if self.OutputCode is not None:
                csv_file = open(os.path.join(self.OutputPath, self.OutputCode + '_' + variable.name + '.csv'), 'w')
            else:
                csv_file = open(os.path.join(self.OutputPath, variable.name + '.csv'), 'w')
            
            # '_implicit_subsets' is an attribute of the pyomo's IndexedComponent
            # class (parent class of Var class). It is a temporary data element that
            # stores sets that are transfered to the model. We use it here to get the 
            # names of the set to use in the csv file's header.
            subsets = getattr(variable, '_implicit_subsets')
            # No index or index made of one set only => subsets is None
            # Index made of at least two sets => subsets is not None
            
            # At least two subsets
            if subsets is not None:
                header = ''
                for subset in subsets:
                    header = header + subset.name + ","
                header = header + variable.name + "\n"
                csv_file.write(header)
                for index in variable:
                    row = ''
                    for item in index:
                        row = row + str(item) + ','
                    row = row + str(value(variable[index])) + "\n"
                    csv_file.write(row)
            
            #index_obj = getattr(variable, '_index')
            #if len(index_obj) > 1:
            
            # 0 or 1 subset
            else:
                # IndexedComponent's method dim() returns the dimension of the index
                # Index made of only one set
                if variable.dim() == 1:
                    header = ''
                    header = header + variable._index.name + "," + variable.name + "\n"
                    csv_file.write(header)
                    for index in variable:
                        row = ''
                        row = row + str(index) + ','
                        row = row + str(value(variable[index])) + "\n"
                        csv_file.write(row)
                # No index
                if variable.dim() == 0:
                    header = ''
                    header = header + variable.name + "\n"
                    csv_file.write(header)
                    for index in variable:
                        row = ''
                        row = row + str(value(variable)) + "\n"
                        csv_file.write(row)
                
            csv_file.close()