#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 16:07:26 2019

@author: mathieusa

This file defines a subclasse of the OSeMOSYS class defined in osemosys.py

The subclasse extends the original OSeMOSYS model and adds the possibility to optimize for 
CO2 emissions instead of costs.
"""

from pyomo.environ import Objective, minimize
from osemosys import abstract_OSeMOSYS

##############################################################################

class abstract_OSeMOSYS_CO2(abstract_OSeMOSYS):
    '''
    Subclass of OSeMOSYS adding minimize CO2 emissions as objective function.
    
    Constructor arguments:
        InputPath [optional]: string
            Path to directory of csv input data files. Default: None.
            
    Public class attributes:
        model: Pyomo AbstractModel object
            Instance of the Pyomo AbstractModel class. Parameter values are unspecified 
            and will be supplied to a concrete model instance (see Class concrete_OSeMOSYS)
            when a solution is to be obtained.
        data: Pyomo DataPortal object
            Instance of the Pyomo DataPortal class. Data from csv files (located 
            at InputPath) can be loaded into this object with the public method load_data().
        InputPath: string
           Path to directory of csv input data files.

    '''    
    def __init__(self, InputPath=None):
        
        super().__init__(InputPath=InputPath)
        
        # Objective function
        self.model.OBJ_co2 = Objective(rule=self.ObjectiveFunctionCO2_rule, sense=minimize, doc='min_CO2')
        
        # For multi-objective models
        self._ObjectiveFunctions = [self.model.OBJ, self.model.OBJ_co2]
        
        # Constraints to be deactivated when minimizing CO2
        self._ConstraintsOnOff = {}
        self._ConstraintsOnOff[self.model.OBJ_co2.name] = [self.model.E8_AnnualEmissionsLimit, self.model.E9_ModelPeriodEmissionsLimit]
        
    def ObjectiveFunctionCO2_rule(self, model):
    	return sum(self.model.ModelPeriodEmissions[r, 'CO2'] for r in self.model.REGION)
