#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Helper script to export all sheets of an Excel file to csv
in a specified directory.
"""

import os
import xlrd
import unicodecsv as csv


def _xlsx2csv(xlsx_file, csv_path):

    workbook = xlrd.open_workbook(xlsx_file)
    sheets = workbook.sheet_names()
    
    for s in sheets:
        
        sheet = workbook.sheet_by_name(s)
        csv_file_name = s + '.csv'
        csvfile = open(os.path.join(csv_path,csv_file_name), 'wb')
        csv_wr = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)
        
        for rownum in range(sheet.nrows):
            row_list = list(x for x in sheet.row_values(rownum))
            for idx, item in enumerate(row_list):
                if isinstance(item, float) and item == int(item):
                    row_list[idx] = int(item)
            csv_wr.writerow(row_list)
        csvfile.close()        

#####
input_xlsx = os.path.join(os.pardir, 'input', 'UTOPIA', 'UTOPIA.xlsx')
csv_path = os.path.join(os.pardir, 'input', 'UTOPIA', 'csv')

_xlsx2csv(input_xlsx, csv_path)