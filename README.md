## osemosys_python_oop

### Purpose

This repo uses code from the Open Source Energy Modeling System (OSeMOSYS) project ([on GitHub](https://github.com/KTH-dESA/OSeMOSYS))
More specifically this repo proposes another way to port the original GNU MathProg code to python, using object oriented programming.

This work was presented as a [lightning talk](https://forum.openmod-initiative.org/t/call-for-lightning-talks-for-aarhus-2019-workshop/1363) at the [2019 Open Energy Modelling Workshop in Aarhus](https://wiki.openmod-initiative.org/wiki/Open_Energy_Modelling_Workshop_-_Aarhus_2019).

### Getting started

The Jupyter notebook `getting_started.ipynb` in the scripts directory shows how to use OSeMOSYS model objects. The notebook is also available as plain HTML at the repo's root directory.